core = 7.x
api = 2

; jalc (jquery-ajax-localstorage-cache) v1.0.4
libraries[jquery-ajax-localstorage-cache][type] = libraries
libraries[jquery-ajax-localstorage-cache][download][type] = git
libraries[jquery-ajax-localstorage-cache][download][url] = git@github.com:SaneMethod/jquery-ajax-localstorage-cache.git
libraries[jquery-ajax-localstorage-cache][download][tag] = 1.0.4
libraries[jquery-ajax-localstorage-cache][directory_name] = jquery-ajax-localstorage-cache
libraries[jquery-ajax-localstorage-cache][destination] = libraries
